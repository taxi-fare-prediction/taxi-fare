import numpy as np
from flask import Flask, request, render_template
import pickle

app = Flask(__name__)
model = pickle.load(open('model.pkl', 'rb'))

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/result')
def result():
    return render_template('predict.html')

@app.route('/predict',methods=['POST'])
def predict():
    if request.method == 'POST':
        p_from = request.form['from']
        p_to = request.form['to']
        km = request.form['km']
        f_price = request.form['fuel_price']
        f_consumption = request.form['fuel_consumption']

        data = [[p_from, p_to, km, f_price, f_consumption]]

        output = model.predict(data)

        price = round(output[0], 2)

        return render_template('predict.html', predicted_price='Price Nu.{}'.format(price))

if __name__ == "__main__":
    app.run(debug=True)